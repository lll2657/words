﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Choose : MonoBehaviour {

	public GameObject button1;
	public GameObject button2;
	public GameObject button3;
	public GameObject button4;
	public GameObject button5;
	public GameObject loading;
	public GameObject loadingLogo;
	public GameObject loadingStar;
	private int i = 0;
	public List<GameObject> objects;
	public GameObject[] availableObjects;
	public float objectsMinX = (float)-9f;
	public float objectsMaxX = -2f;
	public float objectsMinY = -4f;
	public float objectsMaxY = 0f;

	void Start(){
		InvokeRepeating ("AddObject", 1f, 3f);
		button1 = GameObject.Find ("Game");
		button2 = GameObject.Find ("Game 2");
		button5 = GameObject.Find ("Game 3");

		string url = GameConstant.Host+"background";
		Dictionary<string, string> headers = new Dictionary<string, string>();
		headers.Add("Access-Control-Allow-Credentials", "true");
		headers.Add("Access-Control-Allow-Headers", "Accept, X-Access-Token, X-Application-Name, X-Request-Sent-Time");
		headers.Add("Access-Control-Allow-Methods", "GET, POST, OPTIONS");
		headers.Add("Access-Control-Allow-Origin", "*");
		WWW www = new WWW (url, null, headers);
		StartCoroutine (WaitForResponse (1F, www));
		if (www.isDone) {
			var data = JSON.Parse (www.text);
			GameObject image = GameObject.Find ("Canvas");
			if (data ["result"] [0] ["menu"].Value != "") {
				string path = data ["result"] [0] ["menu"].Value;
				string url2 = GameConstant.Host + "bg/" + path;
				WWW www2 = new WWW (url2, null, headers);
				StartCoroutine (WaitForResponse (1F, www2));
				float sizeX = www2.texture.width;
				float sizeY = www2.texture.height;
				Sprite background = Sprite.Create (www2.texture, new Rect (0, 0, sizeX, sizeY), new Vector2 (0.5f, 0.5f));
				image.GetComponent<Image> ().sprite = background;
			}
		}
	}

	IEnumerator WaitForResponse(float timeout, WWW www)
	{
		float time = Time.time;
		while (!www.isDone)
		{
			if (Time.time - time < timeout)
			{
				continue;
			}
			else
			{
				Debug.LogWarning("XmlRpc method timed out");
				break;
			}
		}

		yield return null;
	}

	public void LoadFun()
	{
		GameDataCtx.game = false;
		GameDataCtx.fun = true;
		GameDataCtx.ready = false;
		button1.SetActive (false);
		button2.SetActive (false);
		button5.SetActive (false);
		GameObject Text = GameObject.Find ("Text");
		Text.SetActive (false);
		loading.SetActive (true);
		loadingLogo.SetActive (true);
		loadingStar.SetActive (true);
		StartCoroutine(AsynchronousLoad("scene"));
	}

	public void onClickButton2(){
		button2.GetComponent<Animator> ().Play ("Grow small");
		button1.GetComponent<Animator> ().Play ("Grow small");
		Invoke ("LoadQuestions",0.3f);
	}
	void LoadQuestions()
	{
		GameDataCtx.game = false;
		GameDataCtx.fun = false;
		button1.SetActive(false);
		button2.SetActive(false);
		button5.SetActive (false);
		button3.SetActive(true);
		button4.SetActive(true);
		GameObject image = GameObject.Find ("Image");
		image.GetComponent<Image> ().sprite = Resources.Load<Sprite> ("menu_bg");
	}
	public void LoadWords()
	{
		GameDataCtx.game = true;
		GameDataCtx.fun = false;
		GameDataCtx.ready = false;
		button1.SetActive(false);
		button2.SetActive(false);
		button5.SetActive (false);
		GameObject Text = GameObject.Find ("Text");
		Text.SetActive (false);
		loading.SetActive (true);
		loadingLogo.SetActive (true);
		loadingStar.SetActive (true);
		StartCoroutine(AsynchronousLoad("scene"));
	}
	public void Random()
	{
		GameDataCtx.isRandom = true;
		button3.SetActive(false);
		button4.SetActive(false);
		GameObject Text = GameObject.Find ("Text");
		Text.SetActive (false);
		loading.SetActive (true);
		loadingLogo.SetActive (true);
		loadingStar.SetActive (true);
		StartCoroutine(AsynchronousLoad("scene"));
	}
	public void NotRandom()
	{
		GameDataCtx.isRandom = false;
		button3.SetActive(false);
		button4.SetActive(false);
		GameObject Text = GameObject.Find ("Text");
		Text.SetActive (false);
		loading.SetActive (true);
		loadingLogo.SetActive (true);
		loadingStar.SetActive (true);
		StartCoroutine(AsynchronousLoad("scene"));
	}

	public void quit(){
		Application.Quit ();
	}

	void Update() {
		i -= 1;
		loadingStar.transform.rotation = Quaternion.Euler(0, 0, i);
		foreach (GameObject obj in objects) {
			obj.transform.position = new Vector3 (obj.transform.position.x + (float)0.25, obj.transform.position.y + (float)0.125, 0); 
		}
	}

	void AddObject()
	{
		float random = UnityEngine.Random.Range (1, 3);
		for (int i = 0; i < random; i++) {
			GameObject obj = (GameObject)Instantiate (availableObjects [0]);

			float randomX = UnityEngine.Random.Range (objectsMinX, objectsMaxX);
			float randomY = UnityEngine.Random.Range (objectsMinY, objectsMaxY);
			obj.transform.position = new Vector3 (randomX, randomY, 0); 
			objects.Add (obj);
		}
	}

	IEnumerator AsynchronousLoad (string scene)
	{
		yield return null;
		AsyncOperation ao = SceneManager.LoadSceneAsync(scene);

		while (! ao.isDone)
		{
			float progress = Mathf.Clamp01(ao.progress / 0.9f);
			loading.GetComponent<Text>().text = (int)(progress * 100) + "%";

			yield return null;
		}
	}
}
