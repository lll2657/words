﻿using UnityEngine;
using System;
using System.Collections;
using UnityEngine.UI;

public class Clock : MonoBehaviour {

	public GameObject Top;
	public GameObject Bottom;

	void Update(){
		Top.GetComponent<Image>().fillAmount = 0.5f + GameConstant.timeRemaining / GameConstant.totalTime / 2;
		Bottom.GetComponent<Image>().fillAmount = 1.0f - GameConstant.timeRemaining / GameConstant.totalTime;
	}
}
