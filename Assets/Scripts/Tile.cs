﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Tile : MonoBehaviour
{
    private List<Transform> touchingTiles;
    private AudioSource audSource;

	public float objectsMinX = -8.5f;
	public float objectsMaxX = 8.5f;
	public float objectsMinY = -8.5f;
	public float objectsMaxY = 8.5f;
	public Vector3 originPosition = Vector3.zero;

    private void Awake()
    {
        touchingTiles = new List<Transform>();
        audSource = gameObject.GetComponent<AudioSource>();
    }

    public void PickUp()
    {
		GameObject UI = GameObject.Find ("Scale");
		if (this.gameObject.transform.parent != UI.transform) {
			transform.SetParent (UI.transform, false);
			transform.position = originPosition;
		}
		transform.localScale = new Vector3 (1f, 1f, 1f);
		gameObject.GetComponent<SpriteRenderer> ().sortingOrder = 1;
    }

    public void Drop()
    {
		GameObject UI = GameObject.Find ("Scale");
		originPosition = transform.position;
		if (touchingTiles.Count <= 0) 
		{
			Debug.Log ("Skipped, Count <= 0");
			transform.SetParent(UI.transform,false);
			return;
		}
			transform.localScale = new Vector3 (1f, 1f, 1f);
        gameObject.GetComponent<SpriteRenderer>().sortingOrder = 0;

        Vector2 newPosition;
        var currentCell = touchingTiles[0];
        if (touchingTiles.Count == 1)
        {
            newPosition = currentCell.position;
        }
        else
        {
            var distance = Vector2.Distance(transform.position, touchingTiles[0].position);
            
            foreach (Transform cell in touchingTiles)
            {
                if (Vector2.Distance(transform.position, cell.position) < distance)
                {
                    currentCell = cell;
                    distance = Vector2.Distance(transform.position, cell.position);
                }
            }
            newPosition = currentCell.position;
        }
        if (currentCell.childCount != 0)
        {	
            return;
        }
        else
        {
			transform.SetParent(currentCell,false);
			transform.localScale = new Vector3 (1f, 1f, 1f);
			if(currentCell.parent.parent.name.Equals("Cellrow 1")||currentCell.parent.parent.name.Equals("Cellrow 2")){
				transform.rotation = Quaternion.Euler(0, 0, 0);
			}
			if(currentCell.parent.parent.name.Equals("Cellrow 4")||currentCell.parent.parent.name.Equals("Cellrow 5")){
				transform.rotation = Quaternion.Euler(0, 0, 180);
			}
			if(currentCell.parent.parent.name.Equals("Cellrow 3")){
				transform.rotation = Quaternion.Euler(0, 0, 90);
			}
			if(currentCell.parent.parent.name.Equals("Cellrow 6")){
				transform.rotation = Quaternion.Euler(0, 0, 270);
			}
			audSource.Play();
			transform.position = newPosition;
        }
        
    }

    
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag != "Cell") return;
        if (!touchingTiles.Contains(other.transform))
        {
            touchingTiles.Add(other.transform);
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag != "Cell") return;
        if (touchingTiles.Contains(other.transform))
        {
            touchingTiles.Remove(other.transform);
        }
    }
}
