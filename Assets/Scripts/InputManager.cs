﻿using UnityEngine;
using System.Collections;
using System;

public class InputManager : MonoBehaviour
{
	private bool[] draggingItem = new bool[] {false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false};
	private GameObject[] draggedObject = new GameObject[20];
	private Vector2[] CurrentTouchPosition = new Vector2[20];

	void Update()
	{
		Touch[] myTouches = Input.touches;

		if (!Generator.pause) {
			if (Input.GetMouseButton (0)) {
				CurrentTouchPosition [19] = Camera.main.ScreenToWorldPoint (Input.mousePosition);
				DragOrPickUp (19);
			} else {
				if (draggingItem [19]) {
					DropItem (19);
				}
			}

			for (int i = 0; i < Input.touchCount; i++) {
				CurrentTouchPosition [i] = Camera.main.ScreenToWorldPoint (myTouches [i].position);

				if (myTouches [i].phase == TouchPhase.Began ||
				    myTouches [i].phase == TouchPhase.Moved ||
				    myTouches [i].phase == TouchPhase.Stationary) {
					DragOrPickUp (i);
				}

				if (myTouches [i].phase == TouchPhase.Ended ||
				    myTouches [i].phase == TouchPhase.Canceled) {
					if (draggingItem [i]) {
						DropItem (i);
					}

				}
			}
		}
	}

	private void DragOrPickUp(int i)
	{
		var inputPosition = CurrentTouchPosition[i];
		if (draggingItem[i])
		{
			if (Math.Abs (inputPosition.x - draggedObject [i].transform.position.x) < 1.5 && Math.Abs (inputPosition.y - draggedObject [i].transform.position.y) < 1.5) {
				draggedObject [i].transform.position = inputPosition;
			}
		}
		else
		{
			var layerMask = 1 << 0;
			RaycastHit2D[] touches = Physics2D.RaycastAll(inputPosition, inputPosition, 0.5f, layerMask);
			if (touches.Length > 0)
			{
				var hit = touches[0];
				if (hit.transform != null && hit.transform.tag == "Tile")
				{
					draggingItem[i] = true;
					draggedObject[i] = hit.transform.gameObject;

					hit.transform.GetComponent<Tile>().PickUp();
				}
			}
		}
	}

	void DropItem(int i)
	{
		draggingItem[i] = false;
		draggedObject [i].transform.localScale = new Vector3 (1, 1, 1);
		draggedObject[i].GetComponent<Tile>().Drop();
	}
}