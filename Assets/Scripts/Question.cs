﻿using UnityEngine;
using System.Collections;

public class Question : MonoBehaviour {

	private string question;
	private string img_path;
	private string answer;
	private string distractors;
	private Sprite sprite;

	public Question(string q, string i, string a, string d)
	{
		question = q;
		img_path = i;
		answer = a;
		distractors = d;
	}

	public string Ques {
		get {
			return this.question;
		}
		set {
			this.question = value;
		}
	}

	public string Image {
		get {
			return this.img_path;
		}
		set {
			this.img_path = value;
		}
	}

	public string Answer {
		get {
			return this.answer;
		}
		set {
			this.answer = value;
		}
	}

	public string Distractors {
		get {
			return this.distractors;
		}
		set {
			this.distractors = value;
		}
	}

	public Sprite Sprite {
		get {
			return this.sprite;
		}
		set {
			this.sprite = value;
		}
	}
}
